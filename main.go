package main

import (
	"github.com/gin-gonic/gin"
	_ "go-repo/docs"
	"go-repo/src"
	"go-repo/src/adapters/databases"
	"go-repo/src/configs"
	"go-repo/src/middlewares"
)

func main() {
	// DB
	databases.InitDb()
	r := engine()
	err := r.Run(":8081")
	if err != nil {
		return
	}
}

func engine() *gin.Engine {
	gin.SetMode(gin.ReleaseMode)
	r := gin.New()

	// middle ware
	r.Use(middlewares.DefaultStructuredLogger()) // adds our new middleware
	r.Use(gin.Recovery())

	routers := r.Group("/api/v1")
	src.InitRouter(routers)
	configs.InitDocs(r)
	return r
}
