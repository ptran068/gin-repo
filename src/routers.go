package src

import (
	"github.com/gin-gonic/gin"
	"go-repo/src/domains/users/presenters/routers"
)

func InitRouter(r *gin.RouterGroup) {
	routers.UserRouters(r)
}
