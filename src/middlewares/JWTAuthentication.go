package middlewares

import (
	"github.com/gin-gonic/gin"
	"go-repo/src/domains/users/gateways/models"
	"go-repo/src/domains/users/usecases"
	"go-repo/src/helpers"
	"net/http"
)

// JWTAuthMiddleware middleware for jwt authentication
type JWTAuthMiddleware struct {
	userService usecases.UserService
}

func NewJWTAuthMiddleware(
	userService usecases.UserService,
) JWTAuthMiddleware {
	return JWTAuthMiddleware{
		userService: userService,
	}
}

func (m JWTAuthMiddleware) JwtAuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		token, err := helpers.ExtractToken(c)
		if err != nil {
			c.JSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
			c.Abort()
			return
		}
		UserID, err := helpers.DecryptAccessToken(token)
		if err != nil {
			c.JSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
			c.Abort()
			return
		}
		var user models.User
		err = m.userService.GetUserById(&user, int(UserID.(float64)))
		if err != nil {
			c.JSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
			c.Abort()
			return
		}
		c.Set("user", &user)
		// use c.Get("user") to get user
		c.Next()
	}
}
