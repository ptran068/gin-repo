package databases

import (
	"fmt"
	"github.com/joho/godotenv"
	"go-repo/src/domains/users/gateways/models"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
	"os"
)

var DbInstance *gorm.DB

func InitDb() *gorm.DB {
	DbInstance = ConnectDataBase()
	Migrate()
	return DbInstance
}

func ConnectDataBase() *gorm.DB {

	err := godotenv.Load(".env")

	if err != nil {
		log.Fatalf("Error loading .env file")
	}

	DbHost := os.Getenv("DB_HOST")
	DbUser := os.Getenv("DB_USER")
	DbPassword := os.Getenv("DB_PASSWORD")
	DbName := os.Getenv("DB_NAME")
	DbPort := os.Getenv("DB_PORT")

	DBURL := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", DbUser, DbPassword, DbHost, DbPort, DbName)

	DbInstance, err = gorm.Open(mysql.Open(DBURL), &gorm.Config{})

	if err != nil {
		fmt.Println("Cannot connect to database ")
		log.Fatal("connection error:", err)
	} else {
		fmt.Println("We are connected to the database ")
	}
	return DbInstance
}

func Migrate() {
	DbInstance.AutoMigrate(&models.User{})
	DbInstance.AutoMigrate(&models.RefreshToken{})
	log.Println("Database Migration Completed!")
}
