package helpers

import (
	"github.com/gin-gonic/gin"
)

type Response struct {
	StatusCode int         `json:"status_code"`
	Data       interface{} `json:"data"`
}

func SuccessResponse(c *gin.Context, status int, data interface{}) {
	c.JSON(status, Response{status, data})
}

func ErrorResponse(c *gin.Context, status int, message string) {
	c.JSON(status, gin.H{"status": status, "message": message})
}
