package helpers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	"go-repo/src/domains/users/gateways/models"
	"os"
	"strings"
	"time"
)

type JWT struct {
	privateKey []byte
	publicKey  []byte
}

type AccessTokenClaims struct {
	UserID int
	jwt.StandardClaims
}

type RefreshTokenClaims struct {
	UserID int
	Token  string // UUID
	jwt.StandardClaims
}

func NewJWT(privateKey []byte, publicKey []byte) JWT {
	return JWT{
		privateKey: privateKey,
		publicKey:  publicKey,
	}
}

func EncryptAccessToken(user *models.User) (token string, err error) {
	userID := user.ID
	expirationTime := time.Now().Add(1 * time.Hour)
	claims := AccessTokenClaims{
		int(userID),
		jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
			IssuedAt:  time.Now().Unix(),
		},
	}
	content, err := os.ReadFile("certs/access_private.pem")
	key, err := jwt.ParseRSAPrivateKeyFromPEM(content)
	if err != nil {
		return "", err
	}
	token, err = jwt.NewWithClaims(jwt.SigningMethodRS256, claims).SignedString(key)
	if err != nil {
		return "", fmt.Errorf("create: sign token: %w", err)
	}

	return token, nil
}

func EncryptRefreshToken(user *models.User) (token string, UUIDToken string, err error) {
	userID := user.ID
	UUIDToken = GenerateUUID()
	expirationTime := time.Now().Add(48 * time.Hour) // 2 days
	claims := RefreshTokenClaims{
		int(userID),
		UUIDToken,
		jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
			IssuedAt:  time.Now().Unix(),
		},
	}
	content, err := os.ReadFile("certs/refresh_private.pem")
	key, err := jwt.ParseRSAPrivateKeyFromPEM(content)
	if err != nil {
		return "", "", err
	}
	token, err = jwt.NewWithClaims(jwt.SigningMethodRS256, claims).SignedString(key)
	if err != nil {
		return "", "", fmt.Errorf("create: sign token: %w", err)
	}

	token = UUIDToken + ":" + token

	return token, UUIDToken, nil
}

func DecryptAccessToken(token string) (interface{}, error) {
	content, err := os.ReadFile("certs/access_public.pem")
	key, err := jwt.ParseRSAPublicKeyFromPEM(content)
	if err != nil {
		return "", err
	}
	tok, err := jwt.Parse(token, func(jwtToken *jwt.Token) (interface{}, error) {
		if _, ok := jwtToken.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("unexpected method: %s", jwtToken.Header["alg"])
		}
		return key, nil
	})
	if err != nil {
		return "", fmt.Errorf("validate: %w", err)
	}

	claims, ok := tok.Claims.(jwt.MapClaims)
	if !ok || !tok.Valid {
		return "", fmt.Errorf("validate: invalid")
	}

	return claims["UserID"], nil
}

func DecryptRefreshToken(token string) (interface{}, error) {
	content, err := os.ReadFile("certs/refresh_public.pem")
	key, err := jwt.ParseRSAPublicKeyFromPEM(content)
	if err != nil {
		return "", err
	}
	tok, err := jwt.Parse(token, func(jwtToken *jwt.Token) (interface{}, error) {
		if _, ok := jwtToken.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("unexpected method: %s", jwtToken.Header["alg"])
		}
		return key, nil
	})
	if err != nil {
		return "", fmt.Errorf("validate: %w", err)
	}

	claims, ok := tok.Claims.(jwt.MapClaims)
	if !ok || !tok.Valid {
		return "nil", fmt.Errorf("validate: invalid")
	}

	return claims["UserID"], nil
}

func ExtractToken(c *gin.Context) (token string, err error) {
	bearerToken := c.Request.Header.Get("Authorization")
	if bearerToken == "" || len(strings.Split(bearerToken, " ")) != 2 {
		return "", fmt.Errorf("invalid Token")
	}
	token = strings.Split(bearerToken, " ")[1]
	return token, nil
}

func ExtractRefreshTokenAndUUID(refreshTokenWithUUID string) (UUID string, RefreshToken string, err error) {
	if refreshTokenWithUUID == "" {
		return "", "", fmt.Errorf("invalid Token")
	}
	UUID = strings.Split(refreshTokenWithUUID, ":")[0]
	RefreshToken = strings.Split(refreshTokenWithUUID, ":")[1]
	return UUID, RefreshToken, nil
}
