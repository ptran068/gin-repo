package usecases

import (
	"go-repo/src/adapters/databases"
	models "go-repo/src/domains/users/gateways/models"
	"go-repo/src/domains/users/presenters/dto"
	"go-repo/src/helpers"
	"strconv"
)

// UserRepositoryInterFace represent the user's repository
type UserRepositoryInterface interface {
	CreateUser(user *models.User) (err error)
	GetUsers(users *[]models.User, pagination helpers.Pagination) *helpers.Pagination
	GetUsersByEmail(user *models.User, email string) (err error)
	GetUserById(user *models.User, id int) (err error)
}

type UserServiceInterFace interface {
	CreateUser(user *models.User) (err error)
	AuthJWT(user *models.User) (authJWT dto.AuthJWT, err error)
	GenerateAccessTokenByRefreshToken(data *dto.RefreshTokenInput) (authJWT dto.AuthJWT, err error)
	GetUsers(users *[]models.User, pagination helpers.Pagination) (usersPagination *helpers.Pagination, err error)
	GetUserByEmail(user *models.User, email string) (err error)
}

type RefreshTokenRepositoryInterFace interface {
	CreateToken(refreshToken *models.RefreshToken) (err error)
	DeleteToken(userId uint) (err error)
}

type UserService struct {
	userRepo  UserRepositoryInterface
	tokenRepo RefreshTokenRepositoryInterFace
}

func NewUserService(userRepo UserRepositoryInterface, tokenRepo RefreshTokenRepositoryInterFace) UserServiceInterFace {
	return UserService{
		userRepo:  userRepo,
		tokenRepo: tokenRepo,
	}
}

func (userService UserService) CreateUser(user *models.User) (err error) {
	if err := user.HashPassword(user.Password); err != nil {
		return err
	}
	errs := userService.userRepo.CreateUser(user)
	if errs != nil {
		return errs
	}
	return nil
}

func (userService UserService) AuthJWT(user *models.User) (authJWT dto.AuthJWT, err error) {
	accessToken, err := helpers.EncryptAccessToken(user)
	if err != nil {
		return authJWT, err
	}
	refreshToken, UUID, err := helpers.EncryptRefreshToken(user)
	if err != nil {
		return authJWT, err
	}

	err = userService.tokenRepo.DeleteToken(user.ID)
	if err != nil {
		return dto.AuthJWT{}, err
	}
	newRefreshToken := models.RefreshToken{Token: UUID, UID: user.ID}

	err = userService.tokenRepo.CreateToken(&newRefreshToken)
	if err != nil {
		return authJWT, err
	}

	userData := dto.User{ID: user.ID, Email: user.Email, Username: user.Username}
	authJWT = dto.AuthJWT{UserData: userData, AccessToken: accessToken, RefreshToken: refreshToken}
	return authJWT, nil
}

func (userService UserService) GenerateAccessTokenByRefreshToken(data *dto.RefreshTokenInput) (authJWT dto.AuthJWT, err error) {
	UUID, RefreshToken, err := helpers.ExtractRefreshTokenAndUUID(data.RefreshToken)
	if err != nil {
		return authJWT, err
	}

	err = databases.DbInstance.First(&models.RefreshToken{}, "Token = ?", UUID).Error
	if err != nil {
		return authJWT, err
	}

	UID, err := helpers.DecryptRefreshToken(RefreshToken)
	if err != nil {
		return authJWT, err
	}

	var user models.User
	err = userService.userRepo.GetUserById(&user, int(UID.(float64)))
	if err != nil {
		return dto.AuthJWT{}, err
	}

	authJWT, err = userService.AuthJWT(&user)
	return authJWT, nil
}

func (userService UserService) GetUsers(users *[]models.User, pagination helpers.Pagination) (usersPagination *helpers.Pagination, err error) {
	// avoiding SQL Injection
	if pagination.Sort != "" {
		if _, err = strconv.Atoi(pagination.Sort); err != nil {
			return usersPagination, err
		}
	}
	usersPagination = userService.userRepo.GetUsers(users, pagination)
	return usersPagination, err
}

func (userService UserService) GetUserByEmail(user *models.User, email string) (err error) {
	if err := user.HashPassword(user.Password); err != nil {
		return err
	}
	errs := userService.userRepo.GetUsersByEmail(user, email)
	if errs != nil {
		return errs
	}
	return nil
}

func (userService UserService) GetUserById(user *models.User, id int) (err error) {
	if err := user.HashPassword(user.Password); err != nil {
		return err
	}
	errs := userService.userRepo.GetUserById(user, id)
	if errs != nil {
		return errs
	}
	return nil
}
