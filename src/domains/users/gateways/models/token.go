package models

import (
	"github.com/jinzhu/gorm"
)

type RefreshToken struct {
	gorm.Model
	//ID        string    `gorm:"id" sql:"id" json:"id"`
	User  User   `gorm:"foreignkey:user_id;association_foreignkey:id"`
	UID   uint   `gorm:"column:user_id"`
	Token string `gorm:"uniqueIndex;size:255;not null;" json:"token"`
	//Revoked time.Time `gorm:"size:255;null;" sql:"DEFAULT:NULL"`
}
