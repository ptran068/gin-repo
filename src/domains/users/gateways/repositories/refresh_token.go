package repositories

import (
	"go-repo/src/domains/users/gateways/models"
	"go-repo/src/domains/users/usecases"
	"gorm.io/gorm"
)

type RefreshTokenRepository struct {
	GormDB *gorm.DB
	// redis bla bla,..
}

func NewRefreshTokenRepository(GormDB *gorm.DB) usecases.RefreshTokenRepositoryInterFace {
	return RefreshTokenRepository{
		GormDB: GormDB,
	}
}

func (tokenRepo RefreshTokenRepository) CreateToken(refreshToken *models.RefreshToken) (err error) {
	err = tokenRepo.GormDB.Create(refreshToken).Error
	if err != nil {
		return err
	}
	return nil
}

func (tokenRepo RefreshTokenRepository) DeleteToken(userId uint) (err error) {
	err = tokenRepo.GormDB.Where("user_id", userId).Delete(&models.RefreshToken{}).Error
	if err != nil {
		return err
	}
	return nil
}
