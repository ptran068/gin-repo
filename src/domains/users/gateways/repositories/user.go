package repositories

import (
	"go-repo/src/adapters/databases"
	"go-repo/src/domains/users/gateways/models"
	"go-repo/src/domains/users/usecases"
	"go-repo/src/helpers"
	"gorm.io/gorm"
	"strconv"
)

type UserRepository struct {
	GormDB *gorm.DB
	// redis bla bla,..
}

func NewUserRepository(GormDB *gorm.DB) usecases.UserRepositoryInterface {
	return UserRepository{
		GormDB: GormDB,
	}
}

func (userRepo UserRepository) CreateUser(user *models.User) (err error) {
	err = userRepo.GormDB.Create(user).Error
	if err != nil {
		return err
	}
	return nil
}

func (userRepo UserRepository) GetUsers(users *[]models.User, pagination helpers.Pagination) *helpers.Pagination {
	userRepo.GormDB.Scopes(helpers.Paginate(users, &pagination, databases.DbInstance)).Find(&users)
	pagination.Rows = users
	return &pagination
}

func (userRepo UserRepository) GetUsersByEmail(user *models.User, email string) (err error) {
	err = userRepo.GormDB.First(&user, "Email = ?", email).Error
	if err != nil {
		return err
	}
	return nil
}

func (userRepo UserRepository) GetUserById(user *models.User, id int) (err error) {
	_, err = strconv.Atoi(strconv.Itoa(id))
	if err != nil {
		return err
	}
	err = userRepo.GormDB.First(&user, id).Error
	if err != nil {
		return err
	}
	return nil
}
