package controllers

import (
	"github.com/gin-gonic/gin"
	_ "github.com/swaggo/swag/example/celler/httputil"
	_ "github.com/swaggo/swag/example/celler/model"
	"go-repo/src/domains/users/gateways/models"
	"go-repo/src/domains/users/presenters/dto"
	"go-repo/src/domains/users/usecases"
	"go-repo/src/helpers"
	"net/http"
)

type UserController struct {
	UserService usecases.UserServiceInterFace
}

func NewUserController(userService usecases.UserServiceInterFace) UserController {
	return UserController{UserService: userService}
}

func (userController UserController) CreateUser(c *gin.Context) {
	var user models.User
	if validatedErrors := c.ShouldBindJSON(&user); validatedErrors != nil {
		helpers.ErrorResponse(c, http.StatusBadRequest, validatedErrors.Error())
		return
	}
	err := userController.UserService.CreateUser(&user)
	if err != nil {
		helpers.ErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}
	JWTRes, err := userController.UserService.AuthJWT(&user)
	if err != nil {
		helpers.ErrorResponse(c, http.StatusBadRequest, err.Error())
	}
	helpers.SuccessResponse(c, http.StatusOK, JWTRes)
}

// Login godoc
//	@Summary		Log in to admin account
//	@Description	authentication
//	@ID				get-string-by-int
//	@Accept			json
//	@Produce		json
//	@Param			admin	body	Admin	true	"Add account"

//	@Router	/login [post]

func (userController UserController) Login(c *gin.Context) {
	var data dto.LoginInput
	var user models.User
	if validatedErrors := c.ShouldBindJSON(&data); validatedErrors != nil {
		helpers.ErrorResponse(c, http.StatusBadRequest, validatedErrors.Error())
		return
	}
	if err := userController.UserService.GetUserByEmail(&user, data.Email); err != nil {
		helpers.ErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}
	if err := user.CheckPassword(data.Password); err != nil {
		helpers.ErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}
	JWTRes, err := userController.UserService.AuthJWT(&user)
	if err != nil {
		helpers.ErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}
	helpers.SuccessResponse(c, http.StatusOK, JWTRes)
}

func (userController UserController) GetUsers(c *gin.Context) {
	var users []models.User
	var query helpers.Pagination
	if validatedErrors := c.ShouldBind(&query); validatedErrors != nil {
		helpers.ErrorResponse(c, http.StatusBadRequest, validatedErrors.Error())
		return
	}
	usersPagination, err := userController.UserService.GetUsers(&users, query)
	if err != nil {
		helpers.ErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}
	helpers.SuccessResponse(c, http.StatusOK, usersPagination)
}

func (userController UserController) GenerateNewAccessToken(c *gin.Context) {
	var data dto.RefreshTokenInput
	if validatedErrors := c.ShouldBindJSON(&data); validatedErrors != nil {
		helpers.ErrorResponse(c, http.StatusBadRequest, validatedErrors.Error())
		return
	}
	JWTRes, err := userController.UserService.GenerateAccessTokenByRefreshToken(&data)
	if err != nil {
		helpers.ErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}
	helpers.SuccessResponse(c, http.StatusOK, JWTRes)
}
