package dto

import (
	"go-repo/src/domains/users/gateways/models"
	"time"
)

type RegisterInput struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
	Email    string `json:"email" binding:"required"`
}

type LoginInput struct {
	Email    string `json:"email" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type RefreshTokenInput struct {
	RefreshToken string `json:"refresh_token" binding:"required"`
}

type AuthJWTInput struct {
	User                models.User
	RevokeTokenInstance bool
}

type AuthJWT struct {
	UserData     User
	AccessToken  string
	RefreshToken string
}

type User struct {
	ID        uint      `json:"id" sql:"id"`
	Email     string    `json:"email" validate:"required,email" sql:"email"`
	Username  string    `json:"username" sql:"username"`
	Deleted   bool      `json:"deleted" sql:"deleted"`
	CreatedAt time.Time `json:"createdat" sql:"createdat"`
	UpdatedAt time.Time `json:"updatedat" sql:"updatedat"`
}
