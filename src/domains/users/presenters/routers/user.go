package routers

import (
	"github.com/gin-gonic/gin"
	"go-repo/src/adapters/databases"
	"go-repo/src/domains/users/gateways/repositories"
	"go-repo/src/domains/users/presenters/controllers"
	"go-repo/src/domains/users/usecases"
)

func UserRouters(g *gin.RouterGroup) {
	userRepo := repositories.NewUserRepository(databases.DbInstance)
	tokenRepo := repositories.NewRefreshTokenRepository(databases.DbInstance)
	userService := usecases.NewUserService(userRepo, tokenRepo)
	ctrl := controllers.NewUserController(userService)
	g.Group("users")
	{
		g.GET("/users", ctrl.GetUsers)
		g.POST("/user/register", ctrl.CreateUser)
		g.POST("/user/login", ctrl.Login)
		g.POST("/user/refresh-token", ctrl.GenerateNewAccessToken)
	}
}
