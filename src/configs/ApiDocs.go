package configs

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"go-repo/docs"
)

func InitDocs(r *gin.Engine) {
	// programmatically set swagger info
	docs.SwaggerInfo.Title = "Swagger API V1"
	docs.SwaggerInfo.Description = "Gin Api Docs"
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Host = "localhost:8080"
	docs.SwaggerInfo.BasePath = "/api/v1"
	docs.SwaggerInfo.Schemes = []string{"http", "https"}
	r.GET("/docs/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
}
